﻿using Results.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Results.Providers
{
    public interface IResultProvider
    {
        void Init();

        List<string> GetResults();
    }


    public abstract class BaseResultProvider : IResultProvider
    {
        public abstract List<string> GetResults();

        public virtual void Init()
        {
            using (var db = new ResultContext())
            {
                var org = EnsureOrganizer(db);
                var courseIds = db.Courses.Where(x => x.OrganizerId == org.Id).Select(x => x.Id);
                var brokens = db.Results.Join(db.Courses.Where(x => x.OrganizerId == org.Id), r => r.CourseId, c => c.Id, (r, c) => r)
                                        .Where(x => x.HasData == null).ToList();
                brokens.ForEach(x => db.Results.Remove(x));
                db.SaveChanges();
            }
        }

        protected abstract Organizer EnsureOrganizer(ResultContext db);
    }
}
