﻿using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Results.Models.Data;
using Results.Utils;

namespace Results.Providers
{
    public class Utmb : BaseResultProvider
    {
        private readonly Web web;

        public Utmb()
        {
            web = new Web();
        }

        protected override Organizer EnsureOrganizer(ResultContext db)
        {
            var utmb = db.Organizers.FirstOrDefault(x => x.Code == "utmb");
            if (utmb == null)
            {
                utmb = new Organizer { Code = "utmb", Name = "UTMB® MONT-BLANC", Web = "https://utmbmontblanc.com/en/home" };
                db.Organizers.Add(utmb);
                db.SaveChanges();
            }

            return utmb;
        }

        public override List<string> GetResults()
        {
            var errors = new List<string>();
            Console.WriteLine("LoadCourses:");
            var courses = LoadCourses("https://utmbmontblanc.com/en/page/107/107.html");

            Console.WriteLine("\nLoadResults:");
            courses.OrderByDescending(x => x.Year).ThenBy(x => x.Name).ToList().ForEach(c => errors.AddRange(LoadResults(c)));

            return errors;
        }

        private List<string> LoadResults(Course c)
        {
            var errors = new List<string>();
            Console.WriteLine($"{c.Id}: {c.Year} - {c.Name}");

            var url = $"https://utmbmontblanc.com/result.php?mode=edPass&ajax=false&annee={c.Year}&course={c.Name}";
            var htmlResults = web.Get(url);
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlResults);

            List<Result> finishers = null;

            var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='respTable']/tbody/tr");

            var i = 0;
            var db = new ResultContext();
            rows?.ToList().ForEach(row =>
            {
                var cells = row.SelectNodes(".//*[self::th or self::td]");
                var bid = cells[1].InnerText.Trim();

                if (++i % 70 == 0)
                {
                    db = new ResultContext();
                }

                var notExist = db.Results.Count(x => x.CourseId == c.Id && x.Bib == bid) == 0;
                if (notExist)
                {
                    try
                    {
                        var res = LoadResult(db, c, bid, cells);
                        if (res.HasData == false)
                        {
                            finishers = LoadFromFinishedResult(finishers, c, res);
                        }
                    }
                    catch (OutdateException)
                    {
                        throw;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        errors.Add($"{c.Year}.{c.Name} - {bid}: - {e.Message}");
                    }
                }
            });

            return errors;
        }

        private Result LoadResult(ResultContext db, Course c, string bid, HtmlNodeCollection cells)
        {
            var res = new Result
            {
                CourseId = c.Id,
                GenRank = int.Parse(cells[0].InnerText),

                Bib = bid,
                Name = HtmlEntity.DeEntitize(cells[2].InnerText.Trim()),
                Cat = cells[3].InnerText.Trim(),
            };

            db.Results.Add(res);
            db.SaveChanges();

            LoadResultDetail(res, c, db);
            db.SaveChanges();

            return res;
        }

        private List<Result> LoadFromFinishedResult(List<Result> finishers, Course c, Result res)
        {
            if (finishers == null)
            {
                var url = $"https://utmbmontblanc.com/result.php?mode=edScratch&ajax=false&annee={c.Year}&course={c.Name}";
                var htmlResults = web.Get(url);
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(htmlResults);

                var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='respTable']/tbody/tr")?.ToList() ?? new List<HtmlNode>();
                finishers = rows.Select(row =>
                {
                    var cells = row.SelectNodes(".//*[self::th or self::td]");

                    var idBloc = cells[1].ChildNodes.ToList();
                    var clubSpliter = idBloc.FindIndex(x => x.Name.ToLower() == "br");
                    var names = string.Join("", idBloc.Take(clubSpliter).Select(x => x.InnerText)).Split("-");
                    var club = string.Join("", idBloc.Skip(clubSpliter).Select(x => x.InnerText)).Trim();
                    var bid = names.FirstOrDefault()?.Trim();

                    return new Result
                    {
                        CourseId = c.Id,
                        GenRank = int.Parse(cells[0].InnerText),

                        Bib = bid,
                        Name = HtmlEntity.DeEntitize(names.LastOrDefault()?.Trim()),
                        Club = HtmlEntity.DeEntitize(club),

                        Cat = cells[2].InnerText.Trim(),
                        CatRank = int.Parse(cells[3].InnerText),
                        Time = cells[4].InnerText,
                        Nationality = HtmlEntity.DeEntitize(cells[6].InnerText.Trim())
                    };
                }).ToList();
            }

            var matched = finishers.SingleOrDefault(x => x.Bib == res.Bib);
            if (matched != null)
            {
                res.GenRank = matched.GenRank;
                res.Club = matched.Club;
                res.Cat = matched.Cat;
                res.CatRank = matched.CatRank;
                res.Time = matched.Time;
                res.Nationality = matched.Nationality;
                res.Finisher = true;
            }

            return finishers;
        }

        private void LoadResultDetail(Result res, Course c, ResultContext db)
        {
            Console.WriteLine($"      {c.Year}.{c.Name} : {res.Bib} - {res.Name}");

            var details = new List<ResultDetail>();

            var url = $"https://utmbmontblanc.com/result.php";
            var htmlResults = web.Post(url, new Dictionary<string, object> {
                { "mode", "detail" },
                { "annee", c.Year },
                { "dossard", res.Bib},
                { "course", c.Name},
            });
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlResults);

            var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='respTable']/tbody/tr");
            var maxTime = "";
            rows?.ToList().ForEach(row =>
            {
                var cells = row.SelectNodes(".//*[self::th or self::td]");
                var dateStr = cells[1].InnerText?.Trim();
                if (!string.IsNullOrWhiteSpace(dateStr) && dateStr != "-")
                {
                    var date = cells[1].InnerText.Split("-");
                    if (date.Length != 2) throw new OutdateException();
                    var ranked = int.TryParse(cells[3].InnerText, out var rank);

                    var resd = new ResultDetail
                    {
                        ResultId = res.Id,
                        Point = HtmlEntity.DeEntitize(cells[0].InnerText.Trim()),
                        Day = date[0].Trim(),
                        Clock = date[1].Trim(),
                        Time = cells[2].InnerText.Trim(),
                        Rank = ranked ? rank : (int?)null
                    };

                    if (maxTime.CompareTo(resd.Time) < 0) maxTime = resd.Time;
                    details.Add(resd);
                }
            });
            db.ResultDetails.AddRange(details);


            var info = htmlResults.Split("</h3>", StringSplitOptions.RemoveEmptyEntries).Last().Split(new[] { "<br>", "<br >", "<br />", "<br/>" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var data in info)
            {
                var split = data.IndexOf(":");
                if (split <= 0) continue;

                var key = data.Substring(0, split).Trim().ToLower();
                var val = data.Substring(split + 1).Trim();

                if (string.IsNullOrWhiteSpace(val)) continue;

                if (key.Contains("finisher"))
                {
                    res.Finisher = true;
                    res.Time = val;
                }
                else if (key.Contains("gen") && key.Contains("rank") && int.TryParse(val, out var genRank))
                {
                    res.GenRank = genRank;
                }
                else if (key.Contains("cat") && key.Contains("rank") && int.TryParse(val, out var catRank))
                {
                    res.CatRank = catRank;
                }
                else if (key.Contains("club") || key.Contains("team"))
                {
                    res.Club = HtmlEntity.DeEntitize(val);
                }
                else if (key.Contains("nation") || key.Contains("country"))
                {
                    res.Nationality = HtmlEntity.DeEntitize(val);
                }
            }

            res.HasData = details.Count > 0;
            if (string.IsNullOrWhiteSpace(res.Time))
            {
                res.Time = maxTime;
            }
        }

        private List<Course> LoadCourses(string url)
        {
            var htmlCourses = web.Get(url);
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlCourses);

            var btns = htmlDoc.DocumentNode.SelectNodes("//input[@class='bouton_bleu' and @type='button']");

            using (var db = new ResultContext())
            {
                var utmb = EnsureOrganizer(db);

                btns?.ToList().ForEach(btn =>
                {
                    var onclick = btn.GetAttributeValue("onclick", null);
                    if (onclick != null)
                    {
                        if (!onclick.StartsWith("see('ed',")) throw new OutdateException();
                        Console.WriteLine(onclick);//see('ed','2019','utmb');

                        var data = onclick.Replace("see(", "").Replace(");", "").Replace("'", "").Split(",");
                        var name = data[2];
                        var year = int.Parse(data[1]);
                        var title = btn.ParentNode?.ParentNode?.SelectSingleNode(".//span[@class='col']")?.InnerText;

                        var course = db.Courses.FirstOrDefault(x => x.Year == year && x.Name == name);
                        if (course == null)
                        {
                            course = new Course() { Year = year, Name = name, };
                            db.Courses.Add(course);
                        }
                        course.OrganizerId = utmb.Id;
                        course.Title = HtmlEntity.DeEntitize(string.IsNullOrWhiteSpace(title) ? name : title);
                    }
                });

                db.SaveChanges();

                return db.Courses.AsNoTracking().ToList();
            }
        }
    }
}
