﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Results.Utils
{
    internal class Web
    {
        internal string Get(string url)
        {
            return SetCookies(new WebClient()).DownloadString(url);
        }

        internal string Post(string url, Dictionary<string, object> data)
        {
            string parameters = string.Join("&", data.Select(x => $"{x.Key}={x.Value}"));

            using (var client = new WebClient())
            {
                SetCookies(client);
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                return client.UploadString(url, parameters);
            }
        }

        private WebClient SetCookies(WebClient client)
        {
            client.Headers.Add(HttpRequestHeader.Cookie, "langue_affich=_en");
            return client;
        }
    }
}