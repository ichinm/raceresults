﻿using Microsoft.EntityFrameworkCore;
using Results.Models.Data;
using Results.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Results
{
    public class ResultLoader
    {
        public List<IResultProvider> Providers { get; } = new List<IResultProvider>();

        public ResultLoader()
        {
            Providers.Add(new Utmb());
        }

        public List<string> GetResults()
        {
            var errors = new List<string>();
            Providers.ForEach(p =>
            {
                p.Init();
                errors.AddRange(p.GetResults());
            });

            return errors;
        }
        
        public void Init(bool removeAll)
        {
            using (var db = new ResultContext())
            {
                if (removeAll)
                {
                    db.Database.ExecuteSqlCommand("Truncate table ResultDetails");
                    db.Database.ExecuteSqlCommand("Truncate table Results");
                    db.Database.ExecuteSqlCommand("Truncate table Courses");
                }
            }
        }
    }
}
