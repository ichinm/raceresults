﻿using System;
using System.Runtime.Serialization;

namespace Results
{
    [Serializable]
    internal class OutdateException : Exception
    {
        public OutdateException() : base("Result structure is outdated")
        {
        }

        public OutdateException(string message) : base(message)
        {
        }

        public OutdateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OutdateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}