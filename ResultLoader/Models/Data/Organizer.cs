﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Results.Models.Data
{
    public class Organizer
    {
        public long Id { get; set; }

        [MaxLength(150)]
        public string Code { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Web { get; set; }
    }
}
