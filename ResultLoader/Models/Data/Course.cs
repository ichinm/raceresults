﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Results.Models.Data
{
    public class Course
    {
        public long Id { get; set; }

        public int Year { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Title { get; set; }

        public long OrganizerId { get; set; }
    }
}
