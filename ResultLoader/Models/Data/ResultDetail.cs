﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Results.Models.Data
{
    public class ResultDetail
    {
        public long Id { get; set; }

        public long ResultId { get; set; }

        [MaxLength(200)]
        public string Point { get; set; }

        [MaxLength(50)]
        public string Day { get; set; }

        [MaxLength(50)]
        public string Clock { get; set; }

        [MaxLength(50)]
        public string Time { get; set; }

        public int? Rank { get; set; }
    }
}
