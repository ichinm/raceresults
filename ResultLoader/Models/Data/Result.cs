﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Results.Models.Data
{
    public class Result
    {
        public long Id { get; set; }

        public long CourseId { get; set; }

        [MaxLength(50)]
        public string Bib { get; set; }

        public bool Finisher { get; set; }

        [MaxLength(200)]
        public string Name { get;set;}

        [MaxLength(200)]
        public string Club { get; set; }

        public int? GenRank { get; set; }

        [MaxLength(50)]
        public string Cat { get; set; }

        public int? CatRank { get; set; }

        public string Time { get; set; }

        [MaxLength(150)]
        public string Nationality { get; set; }
        public bool? HasData { get; set; }
    }
}
