﻿using System;
using System.IO;

namespace Results
{
    class Program
    {
        static void Main(string[] args)
        {
            var srv = new ResultLoader();

            srv.Init(false);
            var errors = string.Join("\n", srv.GetResults());

            Console.WriteLine(errors);
            File.WriteAllText($"log-{DateTime.Now.ToString("yy-MM-dd")}.txt", errors);

            Console.WriteLine("\nDone!");
        }
    }
}
